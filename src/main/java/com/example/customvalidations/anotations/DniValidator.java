package com.example.customvalidations.anotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DniValidator implements ConstraintValidator<ValidDni, String> {

	@Override
	public void initialize(ValidDni contactNumber) {
	}

	@Override
	public boolean isValid(String dni, ConstraintValidatorContext cxt) {
		return isNifNumValid(dni);
	}

	// https://es.wikipedia.org/wiki/N%C3%BAmero_de_identificaci%C3%B3n_fiscal
	private boolean isNifNumValid(String nif) {
		// Si el largo del NIF es diferente a 9, acaba el método.
		if (nif.length() != 9) {
			return false;
		}

		String secuenciaLetrasNIF = "TRWAGMYFPDXBNJZSQVHLCKE";
		nif = nif.toUpperCase();

		// Posición inicial: 0 (primero en la cadena de texto).
		// Longitud: cadena de texto menos última posición. Así obtenemos solo el
		// número.
		String numeroNIF = nif.substring(0, nif.length() - 1);

		// Si es un NIE reemplazamos letra inicial por su valor numérico.
		numeroNIF = numeroNIF.replace("X", "0").replace("Y", "1").replace("Z", "2");

		// Obtenemos la letra con un char que nos servirá también para el índice de las
		// secuenciaLetrasNIF
		char letraNIF = nif.charAt(8);
		int i = Integer.parseInt(numeroNIF) % 23;
		return letraNIF == secuenciaLetrasNIF.charAt(i);
	}

}