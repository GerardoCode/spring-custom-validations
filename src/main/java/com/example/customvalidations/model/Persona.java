package com.example.customvalidations.model;

import com.example.customvalidations.anotations.ValidDni;

public class Persona {

	@ValidDni
	private String dni;

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

}
