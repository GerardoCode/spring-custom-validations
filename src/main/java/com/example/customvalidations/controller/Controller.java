package com.example.customvalidations.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.customvalidations.model.Persona;

@RestController
public class Controller {

	@PostMapping("/test")
	public Persona test(@RequestBody @Valid Persona persona) {
		return persona;
	}

}
